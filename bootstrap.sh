#!/bin/bash



###
## install from repo
#

apt install git neovim tmux nodejs fish



###
## install nodejs things
#

# install yarn first
npm install -g yarn
# then install everything else with yarn
yarn global add npx
yarn global add tldr
yarn global add http-server
yarn global add create-elm-app



###
## TODO: change shell to fish and bootstrap fish config
#



###
## symlink dotfiles
#

dotfiles=(".vimrc",".tmux.conf",".gitconfig")
dir="${HOME}/.dotfiles"

for dotfile in "${dotfiles[@]}"; do
	ln -sf "${HOME}/${dotfile}" "${dir}"
	# append this symlink and/or directory to .gitignore
	test -L "${dir}/${dotfile}" && echo ${dotfile} >> .gitignore;
	test -d "${dir}/${dotfile}" && echo ${dotfile} >> .gitignore;
done

